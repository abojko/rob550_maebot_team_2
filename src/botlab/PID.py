# PID.py
#
# skeleton code for University of Michigan ROB550 Botlab
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sys, os, time
import lcm, signal
from math import *

from lcmtypes import odo_pose_xyt_t
from lcmtypes import odo_dxdtheta_t
from lcmtypes import maebot_diff_drive_t
from lcmtypes import maebot_motor_feedback_t

###############################################
# PID CLASS - SIMILAR TO C CODE FOR CTRLPANEL
###############################################
class PID():
	def __init__(self, kp, ki, kd, trimFactor= 0, bias = 0):
		self.kp = kp
		self.ki = ki
		self.kd = kd
		self.trimFactor = trimFactor
		self.bias = bias
		self.iTermFactor = -1.0
		
		self.iTerm = 0.0
		self.iTermMin = -30
		self.iTermMax = 30
		self.prevError = 0.0
		self.input = 0.0
		self.output = 0.0
		self.setpoint = 0.0
		self.outputMin = -2000
		self.outputMax = 2000
		self.updateRate = 0.1
		
		

	def Compute(self):
		self.output = 0.0
	
		# Error
		error = self.setpoint - self.input

		if self.iTermFactor == -1.0 or abs(error) < abs(self.setpoint*self.iTermFactor):
			self.iTerm += self.ki * error 
		
		if(self.iTerm > self.iTermMax):
			self.iTerm = self.iTermMax
		elif(self.iTerm < self.iTermMin):                        
			self.iTerm= self.iTermMin
	

		dTerm = self.kd*(error - self.prevError)
		
		trim = self.trimFactor*self.setpoint
		self.output = self.kp * error + self.iTerm + dTerm
		
		if(self.output > self.outputMax):
			self.output = self.outputMax
		
		elif(self.output < self.outputMin):
			self.output= self.outputMin
		
		self.output = self.output + trim
		
		
		if trim>0:
			self.output=self.output+self.bias
		elif trim<0:
			self.output=self.output-self.bias
		
		self.prevError = error
		print "Out (no trim): %f Error: %f  Set: %f In: %f Out %f Trim %f Bias %f iTerm %f" % (self.output-trim, error, self.setpoint, self.input, self.output, trim, self.bias, self.iTerm)
	


		return self.output
	
	# Only increase iTerm if the error is less than setpoint*iTermFactor (and iTermFactor != 0)
	def SetTunings(self, kp, ki, kd, iTermFactor = -1.0):
		self.kp = kp
		self.ki = ki * self.updateRate
		self.kd = kd / self.updateRate
		self.iTermFactor = iTermFactor

	def SetIntegralLimits(self, imin, imax):
		self.iTermMin = imin
		self.iTermMax = imax

	def SetOutputLimits(self, outmin, outmax):
		self.outputMin = outmin
		self.outputMax = outmax

	def SetUpdateRate(self, rateInSec):
		self.updateRate = rateInSec
