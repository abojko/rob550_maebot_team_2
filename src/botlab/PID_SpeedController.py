import sys, os, time
import lcm, signal
from math import *

from PID import *

from lcmtypes import odo_pose_xyt_t
from lcmtypes import odo_dxdtheta_t
from lcmtypes import maebot_diff_drive_t
from lcmtypes import maebot_motor_feedback_t


###################################################
# PID SPEED CONTROLLER - USED PID CLASS
###################################################

class PIDSpeedController():
	def __init__(self):
		
		self.INIT = 0
		
		# Left wheel
		self.kp_left = 0.1
		self.ki_left = 1.5
		self.kd_left = 0.00
		
		# Right wheel
		self.kp_right = 0.1
		self.ki_right = 1.5
		self.kd_right = 0.00
		
		
		# Create Both PID for controller
		#bias = 58 when the wheels are in the air
		#bias = 160 when on floor
		self.leftWheel = PID(self.kp_left, self.ki_left, self.kd_left, 65.0/94.0, 160)
		self.rightWheel = PID(self.kp_right, self.ki_right, self.kd_right, 65.0/94.0,160)
		
		self.leftWheel.SetOutputLimits(-600, 600)
		self.rightWheel.SetOutputLimits(-600, 600)
		
		self.leftWheel.SetIntegralLimits(-30, 30)
		self.rightWheel.SetIntegralLimits(-30, 30)

		self.prevEncPos = (0,0,0)           # store previous readings for odometry
		self.prevOdoPos = (0,0,0)           # store previous x [mm], y [mm], theta [rad]
		
		self.currEncPos = (0,0,0)           # current reading for odometry
		self.currOdoPos = (0,0,0)           # store odometry x [mm], y [mm], theta [rad]
 
		self.currOdoVel = (0,0,0)
		
		self.d_left = 0
		self.d_right = 0
		
		self.wheelDiameterMillimeters_left = 30.54     # diameter of wheels [mm]
		self.wheelDiameterMillimeters_right = 33.46     # diameter of wheels [mm]
		
		self.axleLengthMillimeters = 67.39        # separation of wheels [mm]  
		self.ticksPerRev = 16.0                  # encoder tickers per motor revolution
		self.gearRatio = 30.0                    # 30:1 gear ratio
		
		self.enc2mm_left = (pi * self.wheelDiameterMillimeters_left) / (self.gearRatio * self.ticksPerRev) # encoder ticks to distance [mm]
		self.enc2mm_right = (pi * self.wheelDiameterMillimeters_right) / (self.gearRatio * self.ticksPerRev) # encoder ticks to distance [mm]

		
		# LCM Subscribe
		self.lc = lcm.LCM()
		lcmOdoVelSub = self.lc.subscribe("BOT_ODO_VEL", self.OdoVelocityHandler)
		lcmOdoMotor = self.lc.subscribe("MAEBOT_MOTOR_FEEDBACK", self.motorFeedbackHandler)
		lcmVelOrderHandler = self.lc.subscribe("BOT_VEL_ORDER", self.OdoVelocityOrderHandler)
		
		signal.signal(signal.SIGINT, self.signal_handler)

		self.rightSpeed = 0.0
		self.leftSpeed = 0.0
		
		self.prevSetpoint = [0,0]
				

		# Solution to check that lcm handle will reveive two msg (pos and vel)
		# before executing further code
		self.msg_counter = [0, 0]
		
		
	def motorFeedbackHandler(self,channel,data):
		msg = maebot_motor_feedback_t.decode(data)

		self.prevEncPos = (self.currEncPos[0], self.currEncPos[1], self.currEncPos[2])	
		self.currEncPos = (msg.encoder_left_ticks, msg.encoder_right_ticks, msg.utime)
				
		if self.INIT == 0:
			self.prevEncPos = (self.currEncPos[0], self.currEncPos[1], self.currEncPos[2])	
			self.INIT = 1
				
		dt = float(self.currEncPos[2]-self.prevEncPos[2])/1000000.0
		
		if dt >= 1e-4:
			rate = dt
		else:
			rate = 1e-4
		
		#print " Encoder %f %f" % (self.currEncPos[0], self.currEncPos[1])
		
		self.leftWheel.SetUpdateRate(rate)
		self.rightWheel.SetUpdateRate(rate)
		
		self.leftWheel.SetTunings(self.kp_left, self.ki_left, self.kd_left)
		self.rightWheel.SetTunings(self.kp_right, self.ki_right, self.kd_right)

		
		self.d_left = self.enc2mm_left*(self.currEncPos[0]-self.prevEncPos[0])/rate
		self.d_right = self.enc2mm_right*(self.currEncPos[1]-self.prevEncPos[1])/rate
		
		self.leftWheel.input = self.d_left
		self.rightWheel.input = self.d_right
		

	def publishMotorCmd(self):
		cmd = maebot_diff_drive_t()
		cmd.motor_right_speed = self.rightSpeed/1300.0*0.93
		cmd.motor_left_speed = self.leftSpeed/1300.0
		
		self.lc.publish("MAEBOT_DIFF_DRIVE", cmd.encode())
		print "Publish: %f %f" % (cmd.motor_right_speed, cmd.motor_left_speed)

	
	def OdoVelocityHandler(self, channel, data):
		self.msg_counter[0] = self.msg_counter[0] + 1
		msg = odo_dxdtheta_t.decode(data)
		
		# Handle velocity message
		self.currOdoVel = (msg.dxy, msg.dtheta, msg.dt)
	
	
	# Listen to ground station orders and change setpoints
	def OdoVelocityOrderHandler(self, channel, data):
		self.msg_counter[1] = self.msg_counter[1] + 1
		msg = maebot_diff_drive_t.decode(data)
		
		if msg.motor_left_speed == 0 and msg.motor_right_speed == 0:
			self.leftWheel.iTerm = 0
			self.rightWheel.iTerm = 0
		
		# Calculate forward velocities and rotation
		self.leftWheel.setpoint = msg.motor_left_speed 
		self.rightWheel.setpoint = msg.motor_right_speed 
		
		#print "From_Ground_setpoint: %f %f" % (msg.motor_left_speed, msg.motor_right_speed)
		

	# Main Program Loop
	def Controller(self):
		while(1):
			self.lc.handle()
			
			if(self.msg_counter[0] * self.msg_counter[1] > 0):
			# MAIN CONTROLLER
				if self.leftWheel.setpoint != self.prevSetpoint[0]:
					self.leftWheel.iTerm = 0
					self.prevSetpoint[0] = self.leftWheel.setpoint
					
				if self.rightWheel.setpoint != self.prevSetpoint[1]:
					self.rightWheel.iTerm = 0
					self.prevSetpoint[1] = self.rightWheel.setpoint

				
				self.leftSpeed = self.leftWheel.Compute()
				self.rightSpeed = self.rightWheel.Compute()
			
				self.msg_counter[0] = self.msg_counter[0] - 1
				self.msg_counter[1] = self.msg_counter[1] - 1
				
				self.publishMotorCmd()
 
	# Function to print 0 commands to morot when exiting with Ctrl+C 
	# No need to change 
	def signal_handler(self,  signal, frame):
		print("Terminating!")
		for i in range(5):
			cmd = maebot_diff_drive_t()
			cmd.motor_right_speed = 0.0
			cmd.motor_left_speed = 0.0	
			self.lc.publish("MAEBOT_DIFF_DRIVE", cmd.encode())
		exit(1)

		
###################################################
# MAIN FUNCTION
###################################################
if __name__ == "__main__":
	pid = PIDSpeedController()
	pid.Controller()
