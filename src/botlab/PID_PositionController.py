import sys, os, time
import lcm, signal
from math import *
from cmath import *

from PID import *

from lcmtypes import rplidar_laser_t
from lcmtypes import pos_cmd_t
from lcmtypes import waypoint_cmd_t
from lcmtypes import order_state_t
from lcmtypes import odo_pose_xyt_t
from lcmtypes import odo_dxdtheta_t
from lcmtypes import maebot_diff_drive_t


###################################################
# PID POSITION CONTROLLER - USED PID CLASS
###################################################

class PIDPositionController():
	
	ORDER_NONE = -1
	ORDER_ABORT = 0
	ORDER_START = 10
	
	WAYPOINT_NONE = -1
	WAYPOINT_ABORT = 0
	WAYPOINT_ADD = 10
		
	def __init__(self):
		
		self.kp_theta = 0.1
		self.ki_theta = 0.0
		self.kd_theta = 0.00
		self.iTermFactorTheta = 0.2
		
		self.kp_fwd = 0.1
		self.ki_fwd = 0
		self.kd_fwd = 0
		self.iTermFactorFwd = 0.2
		
		self.INIT = 0
		
		self.pidTheta = PID(self.kp_theta, self.ki_theta, self.kd_theta)
		self.pidFwd = PID(self.kp_fwd, self.ki_fwd, self.kd_fwd)
		
		self.pidTheta.SetOutputLimits(-200, 200)
		self.pidTheta.SetIntegralLimits(-200, 200)
		
		self.pidFwd.SetOutputLimits(-600, 600)
		self.pidFwd.SetIntegralLimits(-200, 200)
		

		self.prevEncPos = (0,0,0)           # store previous readings for odometry
		self.prevOdoPos = (0,0,0)           # store previous x [mm], y [mm], theta [rad]
		
		self.currEncPos = (0,0,0)           # current reading for odometry
		self.currOdoPos = (0,0,0)           # store odometry x [mm], y [mm], theta [rad]

		self.lastFeedbackTime = 0
		
		self.currentOrder = PIDPositionController.ORDER_NONE
		self.orderStep = 0 # 0: nothing, 1: rotating, 2: advancing
		
		## For position orders
		# Used for the current relative order
		self.startingPosition = (0, 0, 0)
		self.targetPosition = (0, 0) # radius, theta starting from startingPosition
		self.factorLeft = 1
		self.factorRight = 1
		
		## For waypoints
		# List of relative coordinates. Calculate startingPosition / targetPosition from it
		self.waypointQueue = []
		
		# Reference for the absolute coordinates
		self.reference = (0,0)
		
		# LCM Subscribe
		self.lc = lcm.LCM()
		lcmOdoPoseSub = self.lc.subscribe("BOT_ODO_POSE", self.OdoPositionHandler)
		lcmPosOrderSub = self.lc.subscribe("BOT_POS_ORDER", self.OdoPositionOrderHandler)
		lcmWaypointSub = self.lc.subscribe("BOT_WAYPOINT_ORDER", self.OdoWaypointOrderHandler)
		#lcmLidarSub = self.lc.subscribe("RPLIDAR_LASER", self.LidarHandler)
		lcmOdoVelSub = self.lc.subscribe("BOT_ODO_VEL", self.OdoVelocityHandler)

		signal.signal(signal.SIGINT, self.signal_handler)
		
	def OdoVelocityHandler(self, channel, data):
		msg = odo_dxdtheta_t.decode(data)
		self.currOdoVel = (msg.dxy, msg.dtheta, msg.dt)
		
	# Not fully implemented
	def LidarHandler(self,channel,data): 
		msg = rplidar_laser_t.decode(data)
		
		self.points = [(msg.ranges[i],msg.thetas[i]) for i in range(msg.nranges)]
		self.datamatrix.getRobotPos(self.slam.updateSlam([(dist*1000.0, theta*RAD2DEG) for (dist, theta) in self.points], self.currOVel), init=self.init)
		self.datamatrix.drawBreezyMap(self.slam.getBreezyMap())
		data = self.datamatrix		
		
		print data.robot_abs # current robot location
		
		
		self.prevOdoPos = self.currOdoPos
		#self.currOdoPos = (data.robot_abs
		#self.currOdoPos[0
				
		if self.INIT == 0:
			self.prevOdoPos = self.currOdoPos
			self.INIT = 1

		dt = float(msg.utime - self.lastFeedbackTime)/1000000.0
		
		if dt >= 1e-4:
			rate = dt
		else:
			rate = 1e-4

		
		if self.currentOrder == PIDPositionController.ORDER_START:
			self.pidTheta.SetUpdateRate(rate)
			self.pidFwd.SetUpdateRate(rate)
			
			
			self.pidTheta.SetTunings(self.kp_theta, self.ki_theta, self.kd_theta, self.iTermFactorTheta)
			self.pidFwd.SetTunings(self.kp_fwd, self.ki_fwd, self.kd_fwd, self.iTermFactorFwd)
			
			
			self.pidTheta.input = self.currOdoPos[2] - self.startingPosition[2]
			
			# Distance already completed
			self.pidFwd.input = sqrt((self.currOdoPos[0] - self.startingPosition[0])**2 + (self.currOdoPos[1] - self.startingPosition[1])**2)

		self.lastFeedbackTime = time.time()

		
	
	def OdoPositionHandler(self, channel, data):
		msg = odo_pose_xyt_t.decode(data)
		
		self.prevOdoPos = self.currOdoPos
		self.currOdoPos = msg.xyt
				
		if self.INIT == 0:
			self.prevOdoPos = self.currOdoPos
			self.INIT = 1

		dt = float(msg.utime - self.lastFeedbackTime)/1000000.0
		
		if dt >= 1e-4:
			rate = dt
		else:
			rate = 1e-4

		if self.currentOrder == PIDPositionController.ORDER_START:
			self.pidTheta.SetUpdateRate(rate)
			self.pidFwd.SetUpdateRate(rate)
			
			
			self.pidTheta.SetTunings(self.kp_theta, self.ki_theta, self.kd_theta, self.iTermFactorTheta)
			self.pidFwd.SetTunings(self.kp_fwd, self.ki_fwd, self.kd_fwd, self.iTermFactorFwd)
			
			
			self.pidTheta.input = self.currOdoPos[2] - self.startingPosition[2]
			
			# Distance already completed
			self.pidFwd.input = sqrt((self.currOdoPos[0] - self.startingPosition[0])**2 + (self.currOdoPos[1] - self.startingPosition[1])**2)
		
		self.lastFeedbackTime = time.time()

	
	# Listen to ground station orders and change setpoints
	def OdoPositionOrderHandler(self, channel, data):
	
		# Avoid conflicts, prioritize waypoints
		if len(self.waypointQueue) == 0:
			msg = pos_cmd_t.decode(data)
			if msg.order == PIDPositionController.ORDER_ABORT:
				self.currentOrder = PIDPositionController.ORDER_NONE
				self.orderStep = 0
				
			# Ignore messages if there is an active step
			elif msg.order == PIDPositionController.ORDER_START:
				if self.orderStep == 0:				
					self.pidFwd.setpoint = msg.distance 
					self.pidTheta.setpoint = msg.theta
					
					self.currentOrder = msg.order
					self.startingPosition = self.currOdoPos
					self.targetPosition = (msg.distance, msg.theta)


	def OdoWaypointOrderHandler(self, channel, data):
		msg = waypoint_cmd_t.decode(data)
		if msg.order == PIDPositionController.WAYPOINT_ABORT:
			self.waypointQueue = []
			self.currentOrder = PIDPositionController.ORDER_NONE
			self.orderStep = 0
			
		# Ignore messages if there is an active step
		elif msg.order == PIDPositionController.WAYPOINT_ADD:
			self.waypointQueue.append(msg)
				
				
	def publishVelCmd(self):
		vScale = 1000

		cmd = maebot_diff_drive_t()
		
		# Velocities in mm/s
		cmd.motor_right_speed = self.leftSpeed*vScale
		cmd.motor_left_speed = self.rightSpeed*vScale
		
		#print "LS: %f RS: %f" % (cmd.motor_left_speed, cmd.motor_right_speed)
		self.lc.publish("BOT_VEL_ORDER", cmd.encode())
	
	
	def publishOrderState(self):

		msg = order_state_t()
		msg.utime = time.time()
		msg.order = self.currentOrder
		msg.orderStep = self.orderStep
		
		self.lc.publish("BOT_ORDER_STATE", msg.encode())
			
	# Main Program Loop
	def Controller(self):
		while(1):
			self.lc.handle()
			
			# We use the current position and 
			if len(self.waypointQueue) > 0 and self.currentOrder == PIDPositionController.ORDER_NONE:
				w = self.waypointQueue[0]
				self.waypointQueue.pop(0)
				
				relativeTarget = complex(w.x - (self.currOdoPos[0] - self.reference[0]), w.y - (self.currOdoPos[1]- self.reference[1]))
				
				orderR = abs(relativeTarget)
				orderTheta = ((phase(relativeTarget) % (2*pi))-(self.currOdoPos[2] % (2*pi))) % (2*pi)
				
				if orderTheta > pi:
					orderTheta = orderTheta - 2*pi
				elif orderTheta < -pi:
					orderTheta = orderTheta + 2*pi
				
				# Aligns with the circle perimeter.
				self.pidFwd.setpoint = orderR 
				self.pidTheta.setpoint = orderTheta
					
				self.currentOrder = PIDPositionController.ORDER_START
				self.startingPosition = self.currOdoPos
				self.targetPosition = (orderR, orderTheta)	
				
			else:
				self.factorLeft = 1
				self.factorRight = 1
			
			# Rotate then go forward
			if self.currentOrder == PIDPositionController.ORDER_START:
				# The use of a PID in the position controller was replaced by a simple threshold check. It caused instabilities.
				# However, we still use the setpoints in other functions, this is why this variables were not removed.
				
				#deltaTheta = self.pidTheta.Compute()
				#deltaR = self.pidFwd.Compute()				
				
				deltaTheta = self.targetPosition[1] - (self.currOdoPos[2] - self.startingPosition[2])
				deltaR = self.targetPosition[0] - sqrt((self.currOdoPos[0] - self.startingPosition[0])**2 + (self.currOdoPos[1] - self.startingPosition[1])**2)
				
				signTheta = 1
				if self.targetPosition[1] < 0:
					signTheta = -1
				
				if self.orderStep == 0:
					self.pidTheta.iTerm = 0.0
					self.pidFwd.iTerm = 0.0
					self.orderStep = 1
					
				if self.orderStep == 1:
					if signTheta == 1: # Going to the right
					
						# The rotation is close enough, go to forward movement
						if deltaTheta < 5*pi/180:
							self.orderStep = 2

					else:
						if deltaTheta > -5*pi/180:
							self.orderStep = 2
						
				if self.orderStep == 2:
					# Close enough to the goal, ends movement.
					if deltaR < 5 or self.targetPosition[0] == 0:
						self.currentOrder = PIDPositionController.ORDER_NONE
						self.orderStep = 0 # Finished

				
				# Absolute speed for both wheels. Note that the unit is in "motor input" (so between -1.0 and 1.0)
				sp = 0
				
				# Now scale speeds
				if self.orderStep == 1:
				
					# Rotation only 
					sp = 0.10
					self.leftSpeed = sp*signTheta
					self.rightSpeed = -sp*signTheta
					self.publishVelCmd()
				
				elif self.orderStep == 2:
					# Forward only
					# Fast far from the start and far from the end
					if abs(deltaR) > 100 and abs(deltaR - self.targetPosition[0]) > 20 :
						sp = 0.25
					else:
						sp = 0.15
					#print "sp: %f" %sp
					
					self.leftSpeed = sp
					self.rightSpeed = sp
					
					self.publishVelCmd()
				else:
					self.leftSpeed = 0
					self.rightSpeed = 0
					self.publishVelCmd()
			
			# Sends some feedback to ground station about the current commands being carried out
			self.publishOrderState()

	# Function to print 0 commands to morot when exiting with Ctrl+C 
	# No need to change 
	def signal_handler(self,  signal, frame):
		print("Terminating!")
		
		for i in range(5):
			cmd = order_state_t()
			cmd.order = PIDPositionController.ORDER_NONE
			cmd.step = 0
			cmd.utime = time.time()
			
			self.lc.publish("BOT_ORDER_STATE", cmd.encode())
		exit(1)

		
###################################################
# MAIN FUNCTION
###################################################
if __name__ == "__main__":
	pid = PIDPositionController()
	pid.Controller()
