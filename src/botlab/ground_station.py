import sys, os, time
import lcm
import cv2, copy

import math
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.backends.backend_agg as agg
matplotlib.use("Agg")

import pygame
from pygame.locals import *

from PIL import Image
from laser import *
from slam import *
from maps import *

from PID_PositionController import *
from planner import *

from lcmtypes import pos_cmd_t
from lcmtypes import maebot_laser_t
from lcmtypes import waypoint_cmd_t
from lcmtypes import order_state_t
from lcmtypes import maebot_diff_drive_t
from lcmtypes import odo_pose_xyt_t
from lcmtypes import odo_dxdtheta_t
from lcmtypes import rplidar_laser_t


# SLAM preferences
USE_ODOMETRY = True
MAP_QUALITY = 3

# Laser constants
DIST_MIN = 100; # minimum distance
DIST_MAX = 6000; # maximum distance

# Map constants
MAP_SIZE_M = 6.0 # size of region to be mapped [m]
INSET_SIZE_M = 1.0 # size of relative map
MAP_RES_PIX_PER_M = 100 # number of pixels of data per meter [pix/m]
MAP_SIZE_PIXELS = int(MAP_SIZE_M*MAP_RES_PIX_PER_M) # number of pixels across the entire map
MAP_DEPTH = 3 # depth of data points on map (levels of certainty)

# CONSTANTS
DEG2RAD = math.pi / 180
RAD2DEG = 180 / math.pi

# KWARGS
# PASS TO MAP AND SLAM FUNCTIONS AS PARAMETER
KWARGS, gvars = {}, globals()
for var in ['MAP_SIZE_M','INSET_SIZE_M','MAP_RES_PIX_PER_M','MAP_DEPTH','USE_ODOMETRY','MAP_QUALITY']:
  KWARGS[var] = gvars[var] # constants required in modules


  
class MainClass:
	def __init__(self, width=640, height=480, FPS=10):
		pygame.init()
		self.width = width
		self.height = height
		self.screen = pygame.display.set_mode((self.width, self.height))
		

		# LCM Subscribe
		self.lc = lcm.LCM()
		lcmOdoPosSub = self.lc.subscribe("BOT_ODO_POSE", self.OdoPosHandler)
		lcmPosSub = self.lc.subscribe("RPLIDAR_LASER", self.LidarHandler)
		lcmVelSub = self.lc.subscribe("BOT_ODO_VEL", self.OdoVelocityHandler)
		
		lcmOrderStateSub = self.lc.subscribe("BOT_ORDER_STATE", self.OdoOrderStateHandler)

		# Prepare Figure for Lidar
		self.fig = plt.figure(figsize=[3, 3], # Inches
						dpi=100,  # 100 dots per inch, so the resulting buffer is 400x400 pixels
					  )

		self.fig.patch.set_facecolor('white')
		self.fig.add_axes([0,0,1,1],projection='polar')
		self.ax = self.fig.gca()

		self.currOdoPos = odo_pose_xyt_t()
		self.currOdoVel = odo_dxdtheta_t()
		
		self.orderState = order_state_t()
		
		# Declare Laser, datamatrix and slam
		self.laser = RPLidar(DIST_MIN, DIST_MAX) # lidar
		self.datamatrix = DataMatrix(**KWARGS) # handle map data
		self.slam = Slam(self.laser, **KWARGS) # do slam processing

		self.points = []
		self.init = 1
		
		
		canvas = agg.FigureCanvasAgg(self.fig)
		canvas.draw()
		renderer = canvas.get_renderer()
		raw_data = renderer.tostring_rgb()
		size = canvas.get_width_height()
		self.surfobject = pygame.image.fromstring(raw_data, size, "RGB")
		
		
	    	size = self.datamatrix.insetSize_pix
	    	im_init = np.empty((size,size),dtype=np.uint8)
	    	im_init.fill(255)
	    	img = Image.fromarray(im_init)
	    	img.save("./inset.png")
	    	self.img_load = pygame.image.load("./inset.png")

	
	def OdoVelocityHandler(self, channel, data):  
		msg = odo_dxdtheta_t.decode(data)
		self.currOdoVel = msg 
 
	def OdoPosHandler(self, channel, data): 
		msg = odo_pose_xyt_t.decode(data)
		self.currOdoPos = msg
		
		
	def LidarHandler(self,channel,data): 
		msg = rplidar_laser_t.decode(data)
		
		self.points = [(msg.ranges[i],msg.thetas[i]) for i in range(msg.nranges)]
		currOVel = (self.currOdoVel.dxy, self.currOdoVel.dtheta, self.currOdoVel.dt)
		self.datamatrix.getRobotPos(self.slam.updateSlam([(dist*1000.0, theta*RAD2DEG) for (dist, theta) in self.points], currOVel), init=self.init)
		self.datamatrix.drawBreezyMap(self.slam.getBreezyMap())
		#self.datamatrix.saveImage()
		data = self.datamatrix		
		
		#print data.robot_init # x [mm], y [mm], th [deg], defined from lower-left corner of map
		print (3000-data.robot_abs[0],3000-data.robot_abs[1],data.robot_abs[2]) # current robot location
		#print data.robot_rel # robot location relative to start position
		#print data.robot_pix # robot pixel location, relative to upper-left (0,0) of image matrix
		#print "--------"
		
		plt.cla()
		
		# Commented out for performance reasons
		#for point in self.points:
		#	self.ax.plot(point[1],point[0],'or',markersize=2)
		
		INSET = self.datamatrix
		INSET.drawInset()
		img = Image.fromarray(self.datamatrix.insetMatrix)
		
		try:
		    img.save("./inset.png")
		except:
		    print "error saving image"
			# The following is best left commented out when not scanning the maze (for performance reasons).
		    
		    #size = self.datamatrix.insetSize_pix
		    #im_init = np.empty((size,size),dtype=np.uint8)
		    #im_init.fill(255)
		    #img = Image.fromarray(im_init)
		    #img.save("./inset.png")
		    
		try:
		    self.img_load = pygame.image.load("./inset.png")
		except:
		    print "Error reading the inset.PNG file."	
		    
		
		    
		self.ax.set_rmax(1.5)
		self.ax.set_theta_direction(-1)
		self.ax.set_theta_zero_location("N")
		self.ax.set_thetagrids([0,45,90,135,180,225,270,315],labels=['0','45','90','135','180','225','270','315'], frac=None,fmt=None)
		self.ax.set_rgrids([0.5,1.0,1.5],labels=['0.5','1.0',''],angle=None,fmt=None)

		canvas = agg.FigureCanvasAgg(self.fig)
		canvas.draw()
		renderer = canvas.get_renderer()
		raw_data = renderer.tostring_rgb()
		size = canvas.get_width_height()
		self.surfobject = pygame.image.fromstring(raw_data, size, "RGB")
		
		self.init = 0
    
	
	def OdoOrderStateHandler(self, channel, data): 
	    msg = order_state_t.decode(data)
	    self.orderState = msg

	def sendWaypoints(self, img, listOfExtremities):
		wayStack = []
		o = waypoint_cmd_t()
		o.utime = time.time()
		o.order = PIDPositionController.WAYPOINT_ADD
		o.radius = 0
		
		for pair in listOfExtremities:
			size, waypoints = planningofplaner(img, pair[0], pair[1])
			print waypoints
			for w in waypoints:
				
				o.utime = time.time()
				
				# Here, translates the coordinates from the frame of reference of the image to the frame of odometry (180 degree rotation + translation)
				o.x = 10.0*(listOfExtremities[0][0][0]-w[0])
				o.y = 10.0*(listOfExtremities[0][0][1]-w[1])
				
				self.lc.publish("BOT_WAYPOINT_ORDER", o.encode())
				wayStack.append(copy.copy(o))
		
		# Just for fun, add the reverse path
		for i in reversed(wayStack):
			i.utime = time.time()
			self.lc.publish("BOT_WAYPOINT_ORDER", i.encode())

	def MainLoop(self):
		pygame.key.set_repeat(1, 20)
		vScale = 1000

		# Prepare Text to Be output on Screen
		font = pygame.font.SysFont("DejaVuSans Mono",14)		
		plotFrequency = 1
		oldTime = time.time()

		laserState = False
		
		while 1:
			for event in pygame.event.get():
				order = False
				waypoint = False
				
				arrowPressed = False
				leftVel = 0
				rightVel = 0
				
				
				if event.type == pygame.QUIT:
					sys.exit()
					
				elif event.type == KEYDOWN:
					if ((event.key == K_ESCAPE) or (event.key == K_q)):
						sys.exit()
					
					key = pygame.key.get_pressed()
					
					#Note: the angles are counted counter-clockwise
					
					# Rotate +90 degree
					if key[K_a]:
						order = pos_cmd_t()
						order.order = PIDPositionController.ORDER_START
						order.distance = 0
						order.theta = pi/2
						
					# Rotate -90 degree
					elif key[K_d]:
						order = pos_cmd_t()
						order.order = PIDPositionController.ORDER_START
						order.distance = 0
						order.theta = -pi/2
					
					# Advance 1m
					elif key[K_w]:
						order = pos_cmd_t()
						order.order = PIDPositionController.ORDER_START
						order.distance = 1000
						order.theta = 0
					
					# Goes backward 10cm
					elif key[K_s]:
						order = pos_cmd_t()
						order.order = PIDPositionController.ORDER_START
						order.distance = 1000
						order.theta = pi
					
					# Turn +45 degree and advance 30cm
					elif key[K_t]:
						order = pos_cmd_t()
						order.order = PIDPositionController.ORDER_START
						order.distance = 300
						order.theta = pi/4
					
					# Rotates so as to set the absolute angle to zero
					elif key[K_k]:
						order = pos_cmd_t()
						order.order = PIDPositionController.ORDER_START
						order.distance = 0
						order.theta = -self.currOdoPos.xyt[2]
					
					# "Go home function". Rotates then goes straight to the absolute position (0,0)
					elif key[K_h]:
						waypoint = waypoint_cmd_t()
						waypoint.utime = time.time()
						waypoint.order = PIDPositionController.WAYPOINT_ADD
						waypoint.radius = 0
						waypoint.x = 0
						waypoint.y = 0
					
					# Makes a clockwise 1m square
					elif key[K_j]:
						w = waypoint_cmd_t()
						w.utime = time.time()
						w.order = PIDPositionController.WAYPOINT_ADD
						w.radius = 0
						w.utime = time.time()
						
						w.x = 1000
						w.y = 0
						self.lc.publish("BOT_WAYPOINT_ORDER", w.encode())	
						
						w.x = 1000
						w.y = -1000
						self.lc.publish("BOT_WAYPOINT_ORDER", w.encode())	
						
						w.x = 0
						w.y = -1000
						self.lc.publish("BOT_WAYPOINT_ORDER", w.encode())	
			
						w.x = 0
						w.y = 0
						self.lc.publish("BOT_WAYPOINT_ORDER", w.encode())	
					
					# Toggle laser state
					elif key[K_l]:
						laserState = not laserState
						msg = maebot_laser_t()
						msg.laser_power = int(laserState)
						self.lc.publish("MAEBOT_LASER", msg.encode())
					
					# Goes from the start position (in the IMAGE frame of reference) to the goal, using the planner
					elif key[K_m]:
						img = "src/botlab/2015.png"
						start = (299, 90)
						goal = (2,144) #2015
						
						self.sendWaypoints(img, [(start, goal)])
										
					# Aborts any movement, relative orders and empty the waypoint stack
					elif key[K_SPACE]:
						order = pos_cmd_t()
						order.order = PIDPositionController.ORDER_ABORT
						
						waypoint = waypoint_cmd_t()
						waypoint.order = PIDPositionController.WAYPOINT_ABORT
						
						cmd = maebot_diff_drive_t()
						cmd.motor_right_speed = 0
						cmd.motor_left_speed = 0
						cmd.utime = time.time()
						self.lc.publish("BOT_VEL_ORDER", cmd.encode())
					
					# Saves the lidar captured image
					elif key[K_n]:
					    self.datamatrix.saveImage()
					
					if waypoint != False:
					    waypoint.utime = time.time()
					    self.lc.publish("BOT_WAYPOINT_ORDER", waypoint.encode())
					
					if order != False:
						order.utime = time.time()
						self.lc.publish("BOT_POS_ORDER", order.encode())
					
					
					# Manual control
					if key[K_RIGHT]:
						leftVel = leftVel + 0.1
						rightVel = rightVel - 0.1
						arrowPressed = True
						
					elif key[K_LEFT]:
						leftVel = leftVel - 0.1
						rightVel = rightVel + 0.1
						arrowPressed = True
						
					elif key[K_UP]:
						leftVel = leftVel + 0.1
						rightVel = rightVel + 0.1
						arrowPressed = True
						
					elif key[K_DOWN]:
						leftVel = leftVel - 0.1
						rightVel = rightVel - 0.1
						arrowPressed = True
					
					elif key[K_r]:
					    pass
					    #self.lc.publish("BOT_RESET_ODO", "")
					
					if arrowPressed:
						cmd = maebot_diff_drive_t()
						cmd.motor_right_speed = vScale * rightVel
						cmd.motor_left_speed = vScale * leftVel
						cmd.utime = time.time()
						self.lc.publish("BOT_VEL_ORDER", cmd.encode())
						print "(L:%f R:%f)" % (cmd.motor_left_speed, cmd.motor_right_speed)
						
					# Aborts any previous manual speed control order
					if key[K_g] and self.orderState.order == PIDPositionController.ORDER_NONE:
						cmd = maebot_diff_drive_t()
						cmd.motor_right_speed = 0
						cmd.motor_left_speed = 0
						cmd.utime = time.time()
						
						self.lc.publish("BOT_VEL_ORDER", cmd.encode())
						


			# Screen updates
			self.screen.fill((255,255,255))

			# Plot Lidar Scans			
			
            # Causing too many computations, resulting in delays
			#if time.time() - oldTime > 1.0/plotFrequency:
			#	while len(pointsList) >= 1000:
			#		pointsList.pop(0)
			#	
			#	pointsList.append((self.currOdoPos.xyt[0]/1000, self.currOdoPos.xyt[1]/1000))

						
			
			size = self.datamatrix.insetSize_pix
			img_load_scaled = pygame.transform.scale(self.img_load, (2*size,2*size))
			self.screen.blit(img_load_scaled, (0,0))
			

			#for p in pointsList:
			#	self.ax.plot(p[0], p[1],'or',markersize=2)
			
			
			self.screen.blit(self.surfobject, (320,0))
			
			
			# Position and Velocity Feedback Text on Screen
			self.lc.handle()     
			
			pygame.draw.rect(self.screen,(0,0,0),(5,350,300,120),2)
			text = font.render("  Position  ",True,(0,0,0))
			self.screen.blit(text,(10,360))
			
			text = font.render("x: %.2f [mm]" % self.currOdoPos.xyt[0], True,(0,0,0))
			self.screen.blit(text,(10,390))
			
			text = font.render("y: %.2f [mm]" % self.currOdoPos.xyt[1], True,(0,0,0))
			self.screen.blit(text,(10,420))
			
			text = font.render("t: %.2f [deg]" % ((self.currOdoPos.xyt[2]%(2*math.pi))*180/math.pi), True,(0,0,0))
			self.screen.blit(text,(10,450))

			text = font.render("  Velocity  ",True,(0,0,0))
			self.screen.blit(text,(150,360))
			
			# Prevents division by zero
			if self.currOdoVel.dt < 1e-4:
				dtt = 1e8
			else:
				dtt = self.currOdoVel.dt
			
			text = font.render("dxy/dt: %.2f [mm/s]" % (self.currOdoVel.dxy/dtt), True,(0,0,0))
			self.screen.blit(text,(150,390))
			
			text = font.render("dth/dt: %.2f [deg/s]" % (self.currOdoVel.dtheta*180/3.14159/dtt), True,(0,0,0))
			self.screen.blit(text,(150,420))
			
			text = font.render("dt: %.6f [s]" % self.currOdoVel.dt, True,(0,0,0))
			self.screen.blit(text,(150,450))
			
			pygame.display.flip()


MainWindow = MainClass()
MainWindow.MainLoop()
