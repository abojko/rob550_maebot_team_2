# odometry.py
#
# skeleton code for University of Michigan ROB550 Botlab
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sys, os, time
import lcm
from math import *

from breezyslam.robots import WheeledRobot

from lcmtypes import maebot_motor_feedback_t
from lcmtypes import maebot_sensor_data_t
from lcmtypes import odo_pose_xyt_t
from lcmtypes import odo_dxdtheta_t

class Maebot(WheeledRobot):
    
	def __init__(self):
		self.wheelDiameterMillimeters = 32.0     # diameter of wheels [mm]
		self.axleLengthMillimeters = 80.0        # separation of wheels [mm]  
		self.ticksPerRev = 16.0                  # encoder tickers per motor revolution
		self.gearRatio = 30.0                    # 30:1 gear ratio
		self.enc2mm = (pi * self.wheelDiameterMillimeters) / (self.gearRatio * self.ticksPerRev) # encoder ticks to distance [mm]
		self.INIT = 0
		self.w_gyro = 0		# rad/s
		self.w_gyro_bias = 0
		self.INIT_GYRO = 0
		self.flag_bias = 0
		self.gyro_integral = 0  #(rad/s)*us
		self.gyro_integral_init = 0 #(rad/s)*us
		self.gyro_time_init = 0; #us
		self.gyro_time = 0	#us
		self.counter = 0
		self.w_total = 0.0
		self.gy_theta = 0.0
		self.prevtime = 0.0
		self.prevEncPos = (0,0,0)           # store previous readings for odometry
		self.prevOdoPos = (0,0,0)           # store previous x [mm], y [mm], theta [rad]
		self.currEncPos = (0,0,0)           # current reading for odometry
		self.currOdoPos = (0,0,0)           # store odometry x [mm], y [mm], theta [rad]
		self.currOdoVel = (0,0,0)           # store current velocity dxy [mm], dtheta [rad], dt [s]
		WheeledRobot.__init__(self, self.wheelDiameterMillimeters/2.0, self.axleLengthMillimeters/2.0)

		# LCM Initialization and Subscription
		self.lc = lcm.LCM()
		lcmMotorSub = self.lc.subscribe("MAEBOT_MOTOR_FEEDBACK", self.motorFeedbackHandler)
		lcmSensorSub = self.lc.subscribe("MAEBOT_SENSOR_DATA", self.sensorDataHandler)
		lcmResetSub = self.lc.subscribe("BOT_RESET_ODO", self.resetOdo)
  
	def calcVelocities(self):
		# Update self.currOdoVel and self.prevOdoVel
		d_left = self.enc2mm*(self.currEncPos[0]-self.prevEncPos[0])
		d_right = self.enc2mm*(self.currEncPos[1]-self.prevEncPos[1])
		
		dxy = (d_right + d_left)/2.0
		dt = float(self.currEncPos[2]-self.prevEncPos[2])/1000000.0
		dtheta = (d_right - d_left)/self.axleLengthMillimeters
		
		self.prevOdoVel = (self.currOdoVel[0], self.currOdoVel[1], self.currOdoVel[2])
		self.currOdoVel = (dxy, dtheta, dt)

	def getVelocities(self):
		# Return a tuple of (dxy [mm], dtheta [rad], dt [s])
		return self.currOdoVel

	def resetOdo(self, channel, data):
		self.INIT = 0
		self.prevtime = 0.0
		self.prevEncPos = (0,0,0)           # store previous readings for odometry
		self.prevOdoPos = (0,0,0)           # store previous x [mm], y [mm], theta [rad]
		self.currEncPos = (0,0,0)           # current reading for odometry
		self.currOdoPos = (0,0,0)           # store odometry x [mm], y [mm], theta [rad]
		self.currOdoVel = (0,0,0)           # store current velocity dxy [mm], dtheta [rad], dt [s]
		
	def calcOdoPosition(self):
		# Update self.currOdoPos and self.prevOdoPos
		self.prevOdoPos = (self.currOdoPos[0], self.currOdoPos[1], self.currOdoPos[2])
		
		self.calcVelocities()
		
		
		#remove bias from gyro
		Calib_T = 7.0
		w_gyro = 0.0
		T_GYRO = (self.gyro_time - self.gyro_time_init)
		if T_GYRO > Calib_T*1000000.0 and self.flag_bias == 0:
			self.flag_bias = 1;
			self.w_gyro_bias = self.w_total/self.counter
			
			print "Gyro bias: %f" %self.w_gyro_bias
			print "Good to go!"
			
		elif T_GYRO <= Calib_T*1000000.0:
			self.counter = self.counter + 1
			self.w_total = self.w_total + self.w_gyro
			
		elif T_GYRO > Calib_T*1000000.0 and self.flag_bias == 1:
			w_gyro = (self.w_gyro-self.w_gyro_bias)
			self.gy_theta = self.gy_theta + w_gyro*(self.gyro_time-self.prevtime)/1000000.0
		
		self.prevtime = self.gyro_time
		
		'''
		#This was done for testing purposes. Should be used as it uses the integral gyrometry correction, which in fact increases the errors even more.
		if T_GYRO < 1e-5
			dth_gyro = 0
		else:
			dth_gyro = float(self.gyro_integral-self.gyro_integral_init)/1000000.0 - self.w_gyro_bias*T_GYRO/1000000.0
		
		print  "gyro angle: %f" %(180.0*(dth_gyro/pi))
		'''
		
		
		#gyrodometry
		 
		# dt, because self.currOdoVel[2] is dtheta (in rad and not rad/s)
		T = self.currOdoVel[2]
		if T<1e-4:
			T = 1e-4
		
		w_odo = self.currOdoVel[1]/T	
		dg_o = w_gyro - w_odo
		
		if abs(dg_o) > 5*pi/180:
			newtheta = self.prevOdoPos[2] + w_gyro*T
		else:
			newtheta = self.prevOdoPos[2] + w_odo*T
		
		# currOdoVel are not real velocities but dxy, dtheta and dt
		self.currOdoPos = (self.prevOdoPos[0] + self.currOdoVel[0] * cos(newtheta), 
							self.prevOdoPos[1] + self.currOdoVel[0] * sin(newtheta),
							 newtheta)
		

	def getOdoPosition(self):
		# Return a tuple of (x [mm], y [mm], theta [rad])
		return self.currOdoPos

	def publishOdometry(self):
		msg = odo_pose_xyt_t();
		
		msg.utime = time.time()
		msg.xyt = list(self.getOdoPosition())
		
		self.lc.publish("BOT_ODO_POSE", msg.encode())

  
	def publishVelocities(self):
		velocities = self.getVelocities()
	
		msg = odo_dxdtheta_t();
		
		msg.utime = time.time()
		msg.dxy = velocities[0]
		msg.dtheta = velocities[1]
		msg.dt = velocities[2]

		self.lc.publish("BOT_ODO_VEL", msg.encode())

	def motorFeedbackHandler(self,channel,data):
		msg = maebot_motor_feedback_t.decode(data)

		self.prevEncPos = (self.currEncPos[0], self.currEncPos[1], self.currEncPos[2])	
		self.currEncPos = (msg.encoder_left_ticks, msg.encoder_right_ticks, msg.utime)
		
		if self.INIT == 0:
			self.prevEncPos = (self.currEncPos[0], self.currEncPos[1], self.currEncPos[2])	
			self.INIT = 1
		
		# calcOdoPosition() already calls calcVelocities()
		self.calcOdoPosition()
		
		

	def sensorDataHandler(self,channel,data):
		msg = maebot_sensor_data_t.decode(data)

		self.w_gyro = float(msg.gyro[2])*pi/180.0/131.0
		if self.INIT_GYRO == 0:
			self.gyro_time_init = msg.utime
			
			print "Start Calibrating. Please wait 7s"
			self.INIT_GYRO = 1
			
		self.gyro_time = msg.utime
		#self.gyro_integral = float(msg.gyro_int[2])*pi/180.0/131.0
		
	def MainLoop(self):
		oldTime = time.time()
		frequency = 20;
		while(1):
			self.lc.handle()
			if(time.time()-oldTime > 1.0/frequency):
				self.publishOdometry()
				self.publishVelocities()
				oldTime = time.time()   


if __name__ == "__main__":
  
  robot = Maebot()
  robot.MainLoop()  
