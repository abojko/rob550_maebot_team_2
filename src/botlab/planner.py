import numpy as np
import cv2
import time
from math import *
try:
	import Queue as Q  # ver. < 3.0
except ImportError:
	import queue as Q

class planner():
	def __init__(self, start, goal, img_path, printmap = False):
		self.scale = 0.5
		self.start = (int(self.scale*start[0]), int(self.scale*start[1]))
		self.goal = (int(self.scale*goal[0]), int(self.scale*goal[1]))
		
		threshold_value = 205
		self.printmap = printmap
		self.smooth_extent = 10

		self.img = cv2.imread(img_path, 0)
		self.grid = self.img2grid(threshold_value) # scale and binary
		self.heurD = self.heurDeadzone()
		self.maze_rows = len(self.grid)
		self.maze_cols = len(self.grid[0])
		self.parent_node = {}

	def getimgsize(self):
		return self.img.shape
	# to resize and threshold the grid to binary grid
	def img2grid(self, threshold_value):
		small = cv2.resize(self.img, (0,0), fx = self.scale, fy = self.scale, interpolation = cv2.INTER_LINEAR) #INTER_NEAREST, INTER_LINEAR, 
										     							 #INTER_AREA, INTER_CUBIC, INTER_LANCZOS4
		ret, small_threshold = cv2.threshold(small, threshold_value, 255, cv2.THRESH_BINARY)
		if self.printmap:
			cv2.namedWindow('small_threshold', cv2.WINDOW_NORMAL)
			cv2.imshow('small_threshold', small_threshold)
			cv2.imwrite('small_threshold.png', small_threshold)
		return small_threshold

	def smoothen(self, img, scalar, m):
		blur = scalar*cv2.blur(img,(m,m))
		if self.printmap:
			cv2.namedWindow('blur', cv2.WINDOW_NORMAL)
			cv2.imshow('blur', blur)
		return blur
	# heuristic function
	def heur(self, node):
		goal = self.goal
		return abs(goal[0]-node[0]) + abs(goal[1]-node[1])

	# to make the additional cost function to be away from obstacles
	def heurDeadzone(self):
		img_Deadzone = cv2.resize(self.img, (0,0), fx = self.scale, fy = self.scale, interpolation = cv2.INTER_AREA)
		img_Deadzone = self.smoothen(img_Deadzone, 1, self.smooth_extent)
		
		#img_Deadzone = self.smoothen(self.grid, 1, 15)
		rows, cols = img_Deadzone.shape
		for r in range(rows):
			for c in range(cols):	
				img_Deadzone.itemset((r,c), (255-img_Deadzone.item(r,c)))
		if self.printmap:
			cv2.namedWindow('heurD', cv2.WINDOW_NORMAL)
			cv2.imshow('heurD', img_Deadzone)
			cv2.imwrite('heurD.png', img_Deadzone)
		return img_Deadzone
	# to select the waypoints
	def calculateWaypoints(self, path):
		corners = []

		lastDirection = 0 # (1,2,3,4): top, right, bottom, left
		nextDirection = 0
		lastPoint = path[0]

		for p in path[1:]:
				# Between p and the previous point
				diff = (p[0] - lastPoint[0], p[1] - lastPoint[1])
				
				if diff == (0,1):
						direction = 1
				elif diff == (1,0):
						direction = 2
				elif diff == (0, -1):
						direction = 3
				else:
						direction = 4
				#print direction
				# Corner
				if direction != lastDirection:
					lastDirection = direction
					corners.append(lastPoint)
						
				lastPoint = p

		if corners[-1:] != path[-1:]:
			corners.append(path[-1:][0])

		waypoints = [corners[0]]

		# Now we smooth the function
		i = 1
		while i < len(corners):
			j=1
			
			while i+j < len(corners) and sqrt((corners[i+j][0] - corners[i][0])**2 + (corners[i+j][1] - corners[i][1])**2) < 4:
				j += 1
			
			i = i+j
			waypoints.append(corners[i-1])
			
		return waypoints
	# A* using tree search and add a additional cost function 
	def Search(self):
		movement = [(-1, 0), (0 ,1), (0, -1), (1, 0)] #  up right left down
		step_cost = 1
		start = self.start
		OPEN = Q.PriorityQueue()
		g = 0
		h = self.heur(start) + self.heurD.item(start)
		f = g + h
		OPEN.put((f, g, start[0], start[1]))
		CLOSED = set(start)
		found = False

		while 1:
			if OPEN.empty():
				break
			next = OPEN.get()
			f = next[0]
			g = next[1]
			x = next[2]
			y = next[3]
			if self.goal == (x,y):
				found = True
				break
			for i in range(len(movement)):
				x2 = x + movement[i][0]
				y2 = y + movement[i][1]
				if x2 >= 0 and x2 < self.maze_rows and y2 >= 0 and y2 < self.maze_cols and self.grid.item((x2, y2)) != 0:
					if (x2,y2) not in CLOSED:
						CLOSED.add((x2,y2))
						g2 = g + step_cost
						h2 = self.heur((x2,y2)) + self.heurD.item((x2, y2))
						f2 = g2 + h2
						OPEN.put((f2, g2, x2, y2))
						self.parent_node[(x2, y2)] = (x, y)
		scale = self.scale
		if found:
			print "found"
			current = self.goal
			path = [current]
			while current != self.start:
				current = self.parent_node[current]
				path.append(current)
			path.reverse()
			#print len(path)
			if self.printmap:
				for point in path:
					self.grid.itemset(point, 100)
				cv2.namedWindow('aftersearch_map', cv2.WINDOW_NORMAL)
				cv2.imshow('aftersearch_map', self.grid)
				cv2.imwrite('aftersearch_map.png', self.grid)
			path = self.calculateWaypoints(path)
			path = [ (int(r/scale), int(c/scale)) for r, c in path ]
			return path
		else:
			print "no exit"
			return [ (int(self.start[0]/scale), int(self.start[1]/scale)) ]
		
def getimgsize(img_path):
	img = cv2.imread(img_path, 0)
	size = img.shape
	return	(size[0], size[1]) 
# interface for ground station
def planningofplaner(img_path, start, goal):
	P = planner(start, goal, img_path, printmap = 0)
	path = P.Search()
	size = P.getimgsize()
	return (size, path)

if __name__ == "__main__":
	# for test only
	img_path = './2015.png' # need to be executed in src/botlab/
	#img_path = './map1_3x3meters.png'
	#print getimgsize(img_path)
	goal = (2,144) 
	start = (299, 90) #2015
	#start = (297, 144)
	i = time.time()
	(size, waypoints) = planningofplaner(img_path, start, goal)
	f = time.time()
	print f-i
	img = cv2.imread(img_path, 0)	
	for point in waypoints:
		img.itemset(point, 50)
		img.itemset((point[0] + 1, point[1]), 50)
		img.itemset((point[0] + 1, point[1] + 1), 50)
		img.itemset((point[0] + 1, point[1] - 1), 50)
		img.itemset((point[0] - 1, point[1]), 50)
		img.itemset((point[0] - 1, point[1] + 1), 50)
		img.itemset((point[0] - 1, point[1] - 1), 50)
		img.itemset((point[0], point[1] - 1), 50)
		img.itemset((point[0], point[1] + 1), 50)
	img.itemset(start, 100)
	img.itemset(goal, 100)
	cv2.namedWindow('waypoints', cv2.WINDOW_NORMAL)
	cv2.imshow('waypoints', img)
	cv2.imwrite('waypoints.png', img)
	cv2.waitKey(0)
	cv2.destroyAllWindows() 
